FROM golang:alpine AS builder

RUN apk --no-cache add git ca-certificates tzdata && update-ca-certificates
WORKDIR /go/src/gitlab.com/rakshazi/proxyav/
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -v -a -installsuffix cgo -o main

FROM alpine

ARG FRESHCLAM=true
ENV DD_CHECK proxyav

RUN apk --no-cache add ca-certificates tzdata clamav clamav-libunrar netcat-openbsd curl && \
    update-ca-certificates && \
    echo "TCPSocket 3310" >> /etc/clamav/clamd.conf && \
    echo "TCPAddr 127.0.0.1" >> /etc/clamav/clamd.conf && \
    mkdir /var/run/clamav && \
    chown clamav:clamav /var/run/clamav && \
    chmod 750 /var/run/clamav

COPY --from=builder /go/src/gitlab.com/rakshazi/proxyav/main /opt/proxyav/proxyav
COPY docker/entrypoint /opt/proxyav/entrypoint
COPY docker/healthcheck /opt/proxyav/healthcheck
RUN chmod +x /opt/proxyav/entrypoint && \
    chmod +x /opt/proxyav/healthcheck

WORKDIR /opt/proxyav
USER clamav

RUN if [ "$FRESHCLAM" = true ]; then \
        freshclam || echo "ClamAV databases should be updated, but Clam daemon not running, don't worry, it's ok"; \
    fi

HEALTHCHECK --interval=1m --timeout=10s --start-period=5m CMD /opt/proxyav/healthcheck || kill 1
ENTRYPOINT ["/opt/proxyav/entrypoint"]
CMD ["-a", "/usr/bin/clamdscan"]
EXPOSE 8080 3310/tcp
